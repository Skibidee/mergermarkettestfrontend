import React from 'react';

import AppContainer from './components/AppContainer.jsx';

class App extends React.Component {
	constructor(props){
		super(props);
	};

	render(){
		return (
				<div style={{"fontFamily": 'Ubuntu'}}>		    
					<AppContainer />
				</div>
			);
	};
}

export default App;