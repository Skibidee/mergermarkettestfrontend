//Libs
import React from 'react';
import ReactDOM from 'react-dom';
//Components
//Bootstrap
import { Panel, Media, Glyphicon } from 'react-bootstrap';
//Config
//CSS

class NewsArticle extends React.Component {
	constructor(props){
		super(props);
		this.state = {
			newsBodyShown: false
		}
		this.handleToggleNewsArticleBody = this.handleToggleNewsArticleBody.bind(this);
	};

	handleToggleNewsArticleBody(){
		if (this.state.newsBodyShown){
 			this.setState({newsBodyShown: false});
 		} else {
	 		this.setState({newsBodyShown: true});
	 	}
	};

	render(){
		var glyphToUse = "hand-right";
		var colour = "orange";
		var panelStyle = "warning";
		if (this.props.newsArticle.positivityRating > 1){
			glyphToUse = "thumbs-up";
			colour = "green";
			panelStyle = "success";
		}
		if (this.props.newsArticle.positivityRating < 0){
			glyphToUse = "thumbs-down";
			colour = "red";
			panelStyle = "danger";
		}
		return (
			<Panel style={{ "marginBottom": "0"}} collapsible expanded={this.state.newsBodyShown} header={this.props.newsArticle.headline} onClick={this.handleToggleNewsArticleBody}  bsStyle={panelStyle}>			
				<p>{this.props.newsArticle.body} <Glyphicon glyph={glyphToUse} style={{ "color": colour }}/></p>
			</Panel>
			);
	};
}

export default NewsArticle;