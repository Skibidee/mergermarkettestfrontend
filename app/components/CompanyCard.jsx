//Libs
import React from 'react';
import ReactDOM from 'react-dom';
//Components
import NewsArticle from './NewsArticle.jsx'
//Bootstrap
import { Panel, Media } from 'react-bootstrap';
//Config
//CSS

class CompanyCard extends React.Component {
	constructor(props){
		super(props);
		this.state = {
		}
	};

	render(){
		var newsFeedRows = [];
		var count = 0;
		if (this.props.companyCard.newsFeed && this.props.companyCard.newsFeed.length > 0){
			this.props.companyCard.newsFeed.forEach(function(newsArticle){
				count++;
				newsFeedRows.push(<NewsArticle key={count} newsArticle={newsArticle} />);
			});
		}
		var currencyIcon = "";
		switch(this.props.companyCard.stockPriceUnits){
			case "GBP:pence":
				currencyIcon = "£";
				break;
			default:
				currencyIcon = "$";
				break;
		}
		var stockPrice = currencyIcon + this.props.companyCard.stockPrice.toFixed(2);
		if (!this.props.companyCard.stockPrice){
			stockPrice = "No stock price data at this time";
		}
		return (
			
			<Panel style={{ "width": "40%"}}>
				<Media>
					<Media.Body>
						<Media.Heading>{this.props.companyCard.name}</Media.Heading>
						<p>{this.props.companyCard.tickerCode} : {stockPrice}</p>						
						{	newsFeedRows.length > 0 ?
							newsFeedRows
							:
							<p>No company news at this time</p>
						}
					</Media.Body>
				</Media>
			</Panel>
			);
	};
}

export default CompanyCard;