//Libs
import React from 'react';
//Components
import CompanyCard from './CompanyCard.jsx'
//Bootstrap
//Config
import config from '../config/config.js';

class AppContainer extends React.Component {
	constructor(props){
		super(props);
		this.state = {
			companyCards: []
		}
	};

	componentDidMount(){
		var options = {		
            method: "GET",
            url: config.baseUrl + "companyCards",
            contentType: "application/json; charset=UTF-8",
            success: function (data) {
            	this.setState({companyCards: data});
            }.bind(this),
            error: function (err, a, b) {
                console.log(err);
            }.bind(this)
        };
		$.ajax(options);
	}	

	render(){
		var companyCardRows = [];
		var count = 0;
		if (this.state.companyCards && this.state.companyCards.length > 0){
			this.state.companyCards.forEach(function(companyCard){
				count++;
				companyCardRows.push(<CompanyCard key={count} companyCard={companyCard} />);
			});
		}

		return (
				<div>								    
					{companyCardRows}
				</div>
			);
	};
}

export default AppContainer;